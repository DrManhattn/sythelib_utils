#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# The MIT License (MIT)
# Copyright (c) 2020-2020 Zach#2679
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
import nox


@nox.session(python=["3.8"], reuse_venv=True)
def sphinx(session):
    session.run("pip", "install", "-r", "sphinx_requirements.txt")
    session.run(
        "python", "-m", "sphinx.cmd.build", "docs/source", "docs/build", "-b", "html"
    )


@nox.session(python=["3.8"])
def format(session):
    session.install("black")
    session.run(
        "python",
        "-m",
        "black",
        "pylibsythe.py",
        "generate_sythelib_stubs.py",
        "sythelib_utils",
    )
