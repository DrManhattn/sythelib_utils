import sythelib_utils
import types
import textwrap
from collections import defaultdict


def get_name_if_method(possible_method):
    if isinstance(getattr(sythelib_utils.ITEMS, possible_method), types.MethodType):
        return textwrap.indent(f"* {possible_method}", "    ")
    return None


def make_header_and_hlist(header, items):
    category_lines = [
        header,
        "=" * len(header),
        "\n.. hlist::",
        textwrap.indent(":columns: 3\n", "    "),
    ]
    category_lines.extend(
        filter(lambda x: x is not None, [get_name_if_method(x) for x in items])
    )
    return category_lines


if __name__ == "__main__":
    rst_lines = ["===========", "Items Index", "===========\n"]
    elements = [
        x
        for x in dir(sythelib_utils.ITEMS)
        if not x.startswith("_") and not x.endswith("_")
    ]
    categories = defaultdict(list)
    for elem in elements:
        categories[elem[0].upper()].append(elem)
    for category, items in categories.items():
        rst_lines.extend(make_header_and_hlist(category, items))
        rst_lines.append("\n")

    with open("./docs/source/items-index.rst", "w") as fp:
        fp.write("\n".join(rst_lines) + "\n")
