#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# The MIT License (MIT)
# Copyright (c) 2020-2020 Zach#2679
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
__all__ = ["MouseMovementCalculator", "random_move_mouse"]

import math
import random

import typing
from pylibsythe import get_last_mouse_x, get_last_mouse_y, sleep, move_mouse


GRAVITY: typing.Final[int] = 5
WIND: typing.Final[int] = 7
MOUSE_SPEED: typing.Final[int] = 30
TARGET_ERROR: typing.Final[int] = MOUSE_SPEED * 10


class MouseMovementCalculator:
    """
    This standard built-in mouse appears choppy as it takes random 4-16ms breaks while moving to emulate velocity
    and acceleration changes in the mouse movement. It also creates a random mouse path created with gravity and wind
    functions which was built off the old Simba color picker. 1% of the time it will miss its target and correct itself.
    The range it can miss by is randomly calculated off the speed of the mouse and the total distance the mouse
    travelled.
    """

    def __init__(
        self,
        gravity: int = GRAVITY,
        wind: int = WIND,
        mouse_speed: int = MOUSE_SPEED,
        target_error: int = TARGET_ERROR,
    ) -> None:
        self.gravity = gravity
        self.wind = wind
        self.mouse_speed = mouse_speed
        self.target_error = target_error
        self._wind_x = 0
        self._wind_y = 0

    def calc_coords(self, start_coords, end_coords):
        velocity_x, velocity_y, self._wind_x, self._wind_y = 0, 0, 0, 0
        coords_and_delay = []
        xs, ys = start_coords
        xe, ye = end_coords
        total_dist = math.hypot(xs - xe, ys - ye)

        while not math.hypot(xs - xe, ys - ye) < 1:
            velocity_x, velocity_y = self._calc_velocity(
                (xs, ys), (xe, ye), velocity_x, velocity_y, total_dist
            )
            xs += velocity_x
            ys += velocity_y

            w = round(
                max(random.randint(0, max(0, round(100 / self.mouse_speed))) * 6, 5)
                * 0.9
            )
            coords_and_delay.append((int(xs), int(ys), int(w)))
        if round(xe) != round(xs) or round(ye) != round(ys):
            coords_and_delay.append((round(xe), round(ye), 0))

        return coords_and_delay

    def _calc_velocity(
        self, current_coords, end_coords, velocity_x, velocity_y, total_dist
    ):
        xs, ys = current_coords
        xe, ye = end_coords
        dist = math.hypot(xs - xe, ys - ye)
        self.wind = max(min(self.wind, dist), 1)
        random_noise = max(min(round(round(total_dist) * 0.3) / 7, 25), 5)

        if random.randint(0, 5) == 0:
            random_noise = random.randint(2, 3)

        if random_noise <= round(dist):
            max_step = random_noise
        else:
            max_step = round(dist)

        if dist >= self.target_error:
            self._wind_x = self._wind_x / math.sqrt(3) + (
                random.randint(0, round(self.wind) * 2 + 1) - self.wind
            ) / math.sqrt(5)
            self._wind_y = self._wind_y / math.sqrt(3) + (
                random.randint(0, round(self.wind) * 2 + 1) - self.wind
            ) / math.sqrt(5)
        else:
            self._wind_x = self._wind_x / math.sqrt(2)
            self._wind_y = self._wind_y / math.sqrt(2)

        velocity_x = velocity_x + self._wind_x
        velocity_y = velocity_y + self._wind_y

        if dist != 0:
            velocity_x = velocity_x + self.gravity * (xe - xs) / dist
            velocity_y = velocity_y + self.gravity * (ye - ys) / dist

        if math.hypot(velocity_x, velocity_y) > max_step:
            random_dist = max_step / 2 + random.randint(
                0, math.floor(round(max_step) / 2)
            )
            velocity_magnitude = math.sqrt(
                velocity_x * velocity_x + velocity_y * velocity_y
            )
            velocity_x = (velocity_x / velocity_magnitude) * random_dist
            velocity_y = (velocity_y / velocity_magnitude) * random_dist

        return velocity_x, velocity_y


def random_move_mouse(x: int, y: int) -> None:
    """
    A better, more human-like mouse movement algorithm than the built in one from pylibsythe.

    This mouse appears choppy as it takes random 4-16ms breaks while moving to emulate velocity and acceleration
    changes in the mouse movement. It also creates a random mouse path created with gravity and wind functions which
    was built off the old Simba color picker. 1% of the time it will miss its target and correct itself. The range it
    can miss by is randomly calculated off the speed of the mouse and the total distance the mouse travelled.

    :param x: int
        the x coordinate to move to
    :param y: int
        the y coordinate to move to
    :return: None
    """
    _move_mouse(x, y) if random.randint(0, 100) != 0 else _miss_mouse(x, y)


def _move_mouse(x: int, y: int) -> None:
    for x, y, delay in MouseMovementCalculator().calc_coords(
        (get_last_mouse_x(), get_last_mouse_y()), (x, y)
    ):
        move_mouse(x, y)
        sleep(delay)


def _miss_mouse(x: int, y: int) -> None:
    mp = round(math.hypot(get_last_mouse_x() - x, get_last_mouse_y() - y) / 150)
    if mp < 0:
        mp = 1

    miss_x = random.randint(
        max(0, round(x - (MOUSE_SPEED * mp))), round(x + (MOUSE_SPEED * mp))
    )
    miss_y = random.randint(
        max(0, round(y - (MOUSE_SPEED * mp))), round(y + (MOUSE_SPEED * mp))
    )
    _move_mouse(miss_x, miss_y)

    _move_mouse(x, y)
