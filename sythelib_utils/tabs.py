#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# The MIT License (MIT)
# Copyright (c) 2020-2020 Zach#2679
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
__all__ = [
    "MenuTab",
    "COMBAT_OPTIONS",
    "SKILLS",
    "QUEST_LIST",
    "INVENTORY",
    "WORN_EQUIPMENT",
    "PRAYER",
    "MAGIC",
    "CLAN_CHAT",
    "FRIENDS_LIST",
    "ACCOUNT_MANAGEMENT",
    "LOGOUT",
    "OPTIONS",
    "EMOTES",
    "MUSIC_PLAYER",
]

import dataclasses


@dataclasses.dataclass
class MenuTab:
    """Dataclass to represent a menu tab."""

    x: int
    """The tab x coordinate."""
    y: int
    """The tab y coordinate."""


COMBAT_OPTIONS: MenuTab = MenuTab(541, 183)
"""The combat options tab."""
SKILLS: MenuTab = MenuTab(575, 186)
"""The skills tab."""
QUEST_LIST: MenuTab = MenuTab(609, 186)
"""The quest list tab."""
INVENTORY: MenuTab = MenuTab(644, 184)
"""The inventory tab."""
WORN_EQUIPMENT: MenuTab = MenuTab(675, 183)
"""The worn equipment tab."""
PRAYER: MenuTab = MenuTab(708, 185)
"""The prayer tab."""
MAGIC: MenuTab = MenuTab(740, 184)
"""The magic tab."""
CLAN_CHAT: MenuTab = MenuTab(537, 485)
"""The clan chat tab."""
FRIENDS_LIST: MenuTab = MenuTab(577, 484)
"""The friends list tab."""
ACCOUNT_MANAGEMENT: MenuTab = MenuTab(607, 483)
"""The account management tab."""
LOGOUT: MenuTab = MenuTab(643, 483)
"""The logout tab."""
OPTIONS: MenuTab = MenuTab(675, 482)
"""The options tab."""
EMOTES: MenuTab = MenuTab(712, 482)
"""The emotes tab."""
MUSIC_PLAYER: MenuTab = MenuTab(742, 480)
"""The music player tab."""
