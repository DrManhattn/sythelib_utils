#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# The MIT License (MIT)
# Copyright (c) 2020-2020 Zach#2679
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
__all__ = ["AntiBan"]

import random

from pylibsythe import *

from sythelib_utils import inventory
from sythelib_utils import tabs
from sythelib_utils import utils
from sythelib_utils.custom_mouse import random_move_mouse


class AntiBan:
    """
    A basic anti ban class without many functions yet
    """

    def __init__(self, chance: float):
        """
        Randomly cycles between 4 antiban options. Each option has an equal chance of hitting
        with a chance of `chance`

        :param chance: float
            The chance of running anti ban
        """
        if not (0 < chance < 1):
            raise ValueError(
                "Invalid value for chances. Valid values in range: 0 < chances < 1"
            )
        self.chance = chance
        self._anti_ban_options = [
            self._random_sleep,
            self._check_random_skill_xp,
            self._move_off_screen,
            self._alt_tab,
        ]

    def maybe_anti_ban(self) -> None:
        """
        Maybe run the anti ban.

        :return: None
        """
        # first detect if we should run antiban:
        if random.random() < self.chance:
            # pick a random antiban to run
            self._anti_ban_options[random.randint(0, len(self._anti_ban_options) - 1)]()

    @staticmethod
    def _random_sleep() -> None:
        """Randomly takes a break"""
        sleep(random.randint(5_000, 9_000))

    @staticmethod
    def _check_random_skill_xp() -> None:
        """Checks a random skill xp, then goes back to the original tab"""
        scrape_rect(526, 166, 236, 337)
        current_tab = find_color_spiral(0x6B241B)

        skills_tab = tabs.SKILLS
        utils.click_random_circle_point(skills_tab.x, skills_tab.y)
        sleep(random.randint(500, 1_000))

        inventory_slot = inventory.get_inventory_slot(random.randint(0, 27))
        random_move_mouse(inventory_slot.x, inventory_slot.y)
        sleep(random.randint(2_000, 5_000))

        random_move_mouse(current_tab.x, current_tab.y)
        utils.random_click_mouse(current_tab.x, current_tab.y)

    @staticmethod
    def _move_off_screen() -> None:
        if get_last_mouse_x() != 0:
            random_move_mouse(0, random.randint(0, get_game_dims().height))
        unfocus_game_window()
        sleep(random.randint(5_000, 9_000))
        focus_game_window()

    @staticmethod
    def _alt_tab() -> None:
        unfocus_game_window()
        sleep(random.randint(5_000, 9_000))
        focus_game_window()
