#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# The MIT License (MIT)
# Copyright (c) 2020-2020 Zach#2679
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
__all__ = ["Fonts", "Font"]

import typing

from pylibsythe import load_font_from_bitmap, load_mask_from_string

MAGENTA: typing.Final[int] = 0xFF00FF


class Fonts:
    """
    Initializes the three main fonts of runescape so that it is cached in memory.
    It prevents creating multiple instances of the same font which will eventually
    run you out of memory.
    """

    def __init__(self):
        self._font1 = Font.font1()
        self._font2 = Font.font2()
        self._font3 = Font.font3()

    @property
    def font1(self) -> int:
        """
        This is the largest sized RuneScape font used for buttons (like on the login screen).
        """
        return self._font1.get()

    @property
    def font2(self) -> int:
        """
        This is the middle sized RuneScape font used for normal text (chat text, upper left text).
        """
        return self._font2.get()

    @property
    def font3(self) -> int:
        """
        This is the smallest sized RuneScape font used for small text (OCRing stats and inventory numbers).
        """
        return self._font3.get()


class Font:
    """
    A font class which stores the memory value in memory to re-reference.
    DO NOT create multiple of the same font else it will run you out of memory.
    """

    __slots__ = ("memory_index",)

    def __init__(self, memory_index):
        if not isinstance(memory_index, int):
            raise TypeError(
                "Expected int parameter, received %s instead."
                % memory_index.__class__.__name__
            )

        self.memory_index = memory_index

    def get(self) -> int:
        return self.memory_index

    def __str__(self) -> str:
        return "%d".format(self.memory_index)

    def __repr__(self) -> str:
        return "<Font value=%d>" % self.memory_index

    @classmethod
    def font1(cls):
        """
        A factory method that returns a :class:`Font`.
        This is the largest sized RuneScape font used for buttons (like on the login screen).
        """
        return cls(
            load_font_from_bitmap(
                load_mask_from_string(
                    bmp_string="0006#0s8<BLRx4sF+o`-Q(47x#VG&~JzxOjl1Tsn0MG>8;`I`A)<88>;FCyH(+B~8lhq`ZJwQT9Q$~SFDgfVBibPLBf{uX"
                    "@B3YFvY}#8EYDL&NwhDXFa}_g1GZQrnS!P<&p7I@p&EqLMDB76C=O+_L^rN8ZbfXf|9@#YEqgdM|sN1^a#3qwTK}84@Ot`"
                    "V^<Gs&RcGD%KK}L-xySXFzbcvf-$z|@{FS@!XUC^2tktR?SBbWNT+5;j&l$WvdUDn+Z*t>plieeIkDSc_`#%;7WuxLFg7o"
                    "(;l<y|Q#sJG4X@-^YoOqC{OGT!Tr6()^;ju%(5N}GAZP_8(Jj_oCL8h>P(AzLI-Pj@T#U4CuIg146GUTc<3B_>QVieh1cZ"
                    "_8K9()P*ooYGN5tQ|Tg#2h8v2^Gs1UTd&MWTMs#bs$s9a%WPO)WI&^%FeRAY{sQc^E*PM?A$d78p0-nQfuAQOh8ekl9c@7"
                    "mP(o|Z`WN&OtpSo(oHQH?{|>2$mV_CPnFSQAf(ev8BJzM0+Lck?%*u44w|@NmL-%*SpC+zCM1S2l16GwL?NrYf+->*C=8)"
                    "7HjBHpR54!Jjf|}cFqA&~?jKDbe|4`cpDuqlQ*4#6Rw7VoO)AnoX;zm0)E3Bk0Ol(bOC=Q=kk;n8QYaXOp%c&#@dMJg%%P"
                    "+v#kTpbQfiH3V@8WU>f4)AK+F|UwdUov%u8urwCr|y#FD4#fecX=Q5W%dBvXY63|Bl-kN^Mx",
                ),
                characters="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789:-",
                transparent_color=MAGENTA,
            )
        )

    @classmethod
    def font2(cls):
        """
        A factory method that returns a :class:`Font`.
        This is the middle sized RuneScape font used for normal text (chat text, upper left text).
        """
        return cls(
            load_font_from_bitmap(
                load_mask_from_string(
                    bmp_string="0006x0j&TILRx4sF+o`-Q(3=4LO%c%!N4ZK000Ca01yBG0MG^=-~a#s009Fu12h6pRHF>g1tkZSP-RpYR8hxVUB=c^Qj;Y"
                    "qqb(#*h^PsqM99RXj8ilcXoL(JMnRy=H4F)oLMn=)#v>RqM$sE@+gqB(v~5Pl)dfb|7}XU>qhYXOv~9FoV`()dMxwE?sED"
                    "!>fQcnW)f)t8Hj*haF)EE?Y|UcWwv$DQqimXrO=7X2qKj!Qv}!96sH1F_DAvZJi$z&9TUe_mjgYHi(pZaRi7jkeqM25WM$"
                    "v6WA)+?1tw~lk(X9sCOoXDOBN*7&)L9b2rfs5GWVD$!iYb(nGD;gLl0`eNmXa`Pjf)scN=->csI?)DnPSqBNK#FKW)Mq8G"
                    "_t{xDG3mo!z_s<7^KEaLP@a*g(7H(sgTUr%$1guXd@PB6qFc}G>K~=tYo7wv4V_522B>3W`r5GVG;~d8Dk{Gz*L1S%P=BZ"
                    "RMizJV9O+lMN2YRjA+UbQW+9Wu!$OJi5OB;Y=)TyjFCu|A~vIIC}lE6iAxhC3Jn>U#w4VhWXlwrOtDPcB}ye3W{nb(L9|q"
                    "0sU*z|MUvJSDN7?sGb2h<N*a-%P*jl;l%~x_3YH5dD$Izaf{+T6X2NNV$RwhHF>5JNjKY)?C6cNQ5@@MPjBTQgQMQQ0Rg|"
                    "M`LTy<q8&Rrev9{RS%>kIDZJ0KyY)Z{c$+Q}1RkEU+L~Pk=lu_|_BvXY63-lx-@Bjb+",
                ),
                characters="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789:",
                transparent_color=MAGENTA,
            )
        )

    @classmethod
    def font3(cls):
        """
        A factory method that returns a :class:`Font`.
        This is the smallest sized RuneScape font used for small text (OCRing stats and inventory numbers).
        """
        return cls(
            load_font_from_bitmap(
                load_mask_from_string(
                    bmp_string="0001Q05|{&LRx4sF+o`-Q&~)QKpX%8$iM)=000260Pq3<C#WD+)wV??7?F_BCT$uplp7NylVfP8ji9946m3S0vdBc*G-?n"
                    "uXf$IOqKhPzB1SB+q9$NtB`DBA2q?h{l!+wGZDO^ni@744C`cwdAPxWk",
                ),
                characters="1234567890",
                transparent_color=MAGENTA,
            )
        )
