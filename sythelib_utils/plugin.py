#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# The MIT License (MIT)
# Copyright (c) 2020-2020 Zach#2679
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
__all__ = [
    "BaseModel",
    "RuneScapeBaseModel",
    "Animation",
    "Slot",
    "Equipment",
    "GameObjects",
    "Coordinates",
    "Verts",
    "Polys",
    "GroundItems",
    "Health",
    "Inventory",
    "Npcs",
    "Players",
    "Pos",
    "Skill",
    "Skills",
    "TileScene",
    "VarBit",
    "VarPlayer",
    "AddMenuEntry",
    "RemoveMenuEntry",
]

import abc
import typing
from dataclasses import is_dataclass, asdict, dataclass
from enum import Enum

import requests
from dacite import from_dict

ThisImplT = typing.TypeVar("ThisImplT")


class BaseModel(metaclass=abc.ABCMeta):
    """Base marshalling functionality for any model we use with Dacite"""

    @classmethod
    def from_dict(cls: typing.Type[ThisImplT], obj: dict) -> ThisImplT:
        return from_dict(cls, obj)

    def to_dict(self):
        assert is_dataclass(self)
        # noinspection PyDataclass
        return asdict(self)

    def __getitem__(self, item):
        return getattr(self, item)

    def __iter__(self):
        for attr, value in self.__dict__.items():
            yield attr, value


class RuneScapeBaseModel(BaseModel):
    """Base marshalling functionality any object based API call"""
    s: requests.Session = requests.Session()
    port: int = 8081

    @staticmethod
    def set_port(port: int) -> None:
        """
        Sets the port to the port sythelib is running on. Defaults to 8081.

        :param port: int
            The port to change to.
        :return: None
        """
        RuneScapeBaseModel.port = port

    @staticmethod
    def get_port() -> int:
        """
        Get's the current port the api is grabbing from.

        :return: int
            The current port.
        """
        return RuneScapeBaseModel.port

    @classmethod
    def get(cls: typing.Type[ThisImplT]) -> ThisImplT:
        """
        A get method inherited by all subclasses. This grabs the info from the API.

        :return: ThisImplT
            The dataclass that inherits this class.
        """
        url = f"http://localhost:{RuneScapeBaseModel.port}/{cls.__name__.lower()}"
        r = RuneScapeBaseModel.s.get(url)
        return from_dict(data_class=cls, data=r.json())


class RuneScapeListBaseModel(RuneScapeBaseModel):
    """
    A get method inherited by all subclasses. This grabs the info from the API.

    :return: typing.List[ThisImplT]
        The dataclass that inherits this class.
    """

    @classmethod
    def get(cls: typing.Type[ThisImplT]) -> typing.List[ThisImplT]:
        url = f"http://localhost:{RuneScapeBaseModel.port}/{cls.__name__.lower()}"
        r = RuneScapeBaseModel.s.get(url)
        return [from_dict(data_class=cls, data=o) for o in r.json()]


@dataclass()
class Animation(RuneScapeBaseModel):
    """An animation dataclass"""
    animation: int
    """The animation id"""
    spotAnimation: int
    """The spot animation id"""
    pose: int
    """The pose id"""
    poseIdle: int
    """The idle pose id"""


@dataclass()
class Slot(BaseModel):
    """An equipment slot dataclass"""

    id: int
    """The slot id"""
    amount: int
    """The amount of items in that slot"""
    name: str
    """The name of the item"""
    slot: int
    """Which slot the item is in from 0-27"""


@dataclass()
class Equipment(RuneScapeBaseModel):
    """An equipment dataclass"""

    head: Slot
    """The head slot"""
    cape: Slot
    """The cape slot"""
    amulet: Slot
    """The amulet slot"""
    weapon: Slot
    """The weapon slot"""
    torso: Slot
    """The torso slot"""
    shield: Slot
    """The shield slot"""
    legs: Slot
    """The legs slot"""
    gloves: Slot
    """The gloves slot"""
    boots: Slot
    """The boots slot"""
    ring: Slot
    """The ring slot"""
    ammo: Slot
    """The ammo slot"""


@dataclass()
class Coordinates(BaseModel):
    """Basic coordinate dataclass"""
    x: int
    """x coordinate"""
    y: int
    """y coordinate"""


@dataclass()
class Verts(BaseModel):
    """A verts dataclass which holds points of a polygon"""
    verts: typing.List[typing.List[int]]
    """The points of a polygon"""


@dataclass()
class Polys(BaseModel):
    """A dataclass for polygons"""
    polys: typing.List[Verts]
    """The polygons"""


@dataclass()
class GameObjects(RuneScapeListBaseModel):
    """A game object dataclass"""
    id: int
    """The id of the object"""
    name: str
    """The name of the object"""
    pos: Coordinates
    """The coordinates of the object"""
    canvas: typing.Optional[Polys]
    """The clickbox canvas of the item"""


@dataclass()
class GroundItems(RuneScapeListBaseModel):
    """A dataclass for the ground items"""
    id: int
    """The item id"""
    quantity: int
    """The quantity of the item"""
    name: str
    """The name of the item"""
    pos: Coordinates
    """The coordinates of the item"""
    canvas: typing.Optional[Polys]
    """The clickbox canvas of the item"""


@dataclass()
class Health(RuneScapeBaseModel):
    """A Health dataclass"""
    health: int
    """Your current hp"""


@dataclass()
class Inventory(RuneScapeBaseModel):
    """A dataclass of an Inventory"""
    items: typing.List[Slot]
    """The items in your inventory"""


@dataclass()
class Npcs(RuneScapeListBaseModel):
    """A dataclass of NPCs"""
    name: typing.Optional[str]
    """The name"""
    combatLvl: int
    """The combat level"""
    healthRatio: int
    """The health ratio"""
    healthScale: int
    """The health scale"""
    interacting: str
    """Who the npc is interacting with"""
    pos: Coordinates
    """The coordinates"""
    index: int
    """The index"""
    canvas: typing.Optional[Polys]
    """The clickable canvas of the npc"""


@dataclass()
class Players(RuneScapeListBaseModel):
    """The Players dataclass"""
    localPlayer: bool
    """Whether it's a local player or not"""
    name: typing.Optional[str]
    """The name"""
    id: int
    """The id"""
    pos: Coordinates
    """The coordinates"""
    healthRatio: int
    """The health ratio"""
    healthScale: int
    """The health scale"""
    level: int
    """The level"""
    interacting: str
    """Who the player is interacting with"""
    interactingID: int
    """The interacting id"""
    overhead: int
    """The overhead"""
    skulled: bool
    """Whether the player is skulled"""
    equipment: Equipment
    """The equipment the player is wearing"""
    canvas: typing.Optional[Polys]
    """The clickbox of the player"""


@dataclass()
class Pos(RuneScapeBaseModel, Coordinates):
    """The position of the player"""
    pass


@dataclass()
class Skill(BaseModel):
    """The skill dataclass"""
    level: int
    """The level"""
    boostedLevel: int
    """The boosted level"""
    xp: float
    """The current xp"""


@dataclass()
class Skills(RuneScapeBaseModel):
    """A skills dataclass"""
    Attack: Skill
    """The attack level"""
    Defense: Skill
    """The defense level"""
    Strength: Skill
    """The strength level"""
    Hitpoints: Skill
    """The hitpoints level"""
    Ranged: Skill
    """The ranged level"""
    Prayer: Skill
    """The prayer level"""
    Magic: Skill
    """The magic level"""
    Cooking: Skill
    """The cooking level"""
    Woodcutting: Skill
    """The woodcutting level"""
    Fletching: Skill
    """The fletching level"""
    Fishing: Skill
    """The fishing level"""
    Firemaking: Skill
    """The firemaking level"""
    Crafting: Skill
    """The crafting level"""
    Smithing: Skill
    """The smithing level"""
    Mining: Skill
    """The mining level"""
    Herblore: Skill
    """The herblore level"""
    Agility: Skill
    """The agility level"""
    Thieving: Skill
    """The thieving level"""
    Slayer: Skill
    """The slayer level"""


@dataclass()
class TileScene(RuneScapeBaseModel, Coordinates):
    """The tile scene of the player"""
    pass


@dataclass()
class VarBit(RuneScapeBaseModel):
    """A var bit dataclass"""
    value: int
    """The int value result"""

    @classmethod
    def get(cls: typing.Type[ThisImplT], varbit: Enum) -> ThisImplT:
        """
        A var bit is useful information stored in the client on the player.

        :param varbit: int
            the int of the var bit you're looking for
        :return: ThisImplT
            the value of the varbit
        """
        url = f"http://localhost:{RuneScapeBaseModel.port}/{cls.__name__.lower()}"
        r = RuneScapeBaseModel.s.get(url, params={"varbit": varbit.value})
        return from_dict(data_class=cls, data=r.json())


@dataclass()
class VarPlayer(RuneScapeBaseModel):
    """A var player dataclass"""
    value: int
    """The value of the result"""

    @classmethod
    def get(cls: typing.Type[ThisImplT], varplayer: Enum) -> ThisImplT:
        """
        A var player is useful information stored in the client on the player.

        :param varplayer: int
            the int of the var bit you're looking for
        :return: ThisImplT
            the value of the varplayer
        """
        url = f"http://localhost:{RuneScapeBaseModel.port}/{cls.__name__.lower()}"
        r = RuneScapeBaseModel.s.get(url, params={"varplayer": varplayer.value})
        return from_dict(data_class=cls, data=r.json())


@dataclass()
class AddMenuEntry(RuneScapeBaseModel):
    """Adds a menu entry. Ths is useful when you want to be able to left click to use an option"""
    success: str
    """The success message of the menu entry"""

    @classmethod
    def get(cls: typing.Type[ThisImplT], target: str, option: str, priority: int = 100) -> ThisImplT:
        """
        This will update the left click option for example if you wish to left click to use an item instead of eating.

        :param target: str
            The name of the item
        :param option: str
            The option to change
        :param priority: int
            the priority on the option. Defaults to 100
        :return: ThisImplT
            the AddMenuEntry dataclass
        """
        url = f"http://localhost:{RuneScapeBaseModel.port}/{cls.__name__.lower()}"
        r = requests.get(url, params={"target": target, "option": option, "priority": priority})
        return from_dict(data_class=cls, data=r.json())


@dataclass()
class RemoveMenuEntry(RuneScapeBaseModel):
    """A menu entry remover dataclass"""
    success: str
    """The success message"""

    @classmethod
    def get(cls: typing.Type[ThisImplT], target, option) -> ThisImplT:
        """
        This will revert the left click option that you added before.

        :param target: str
            The name of the item
        :param option: str
            The option to change
        :return: ThisImplT
            The RemoveMenuEntry dataclass
        """
        url = f"http://localhost:{RuneScapeBaseModel.port}/{cls.__name__.lower()}"
        r = requests.get(url, params={"target": target, "option": option})
        return from_dict(data_class=cls, data=r.json())
